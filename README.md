# README #

This repo contains a C library for the AD5328 DAC.

### Highlights ###

* 12 bit
* 2.5V to 5.5V output
* Octal outputs  
* [Datasheet](https://www.analog.com/media/en/technical-documentation/data-sheets/AD5308_5318_5328.pdf)

### Usage ###

Clone this repo as a subrepo in you CMAKE project and reference the folder.

### Important ###

Data is clocked on the FALING edge of the SPI clock.
Clock is idle LOW
