#include <stdint.h>
#include <stdbool.h>
#include "LL_spi.h"

struct AD5328_t
 {
	float VoltageADivider;
	float VoltageBDivider;
	uint8_t ChipID;
    //points to a function to handle the SPI read/writes
    LL_SPIMaster_ReadWriteMethod_t SPIFunction;
};

#define AD5328_BUFFERED_REFA  4
#define AD5328_BUFFERED_REFB  8
#define AD5328_VCC_AS_REFA  1
#define AD5328_VCC_AS_REFB  2
#define AD5328_REFA_DOUBLE_GAIN  16
#define AD5328_REFB_DOUBLE_GAIN  32

int AD5328_Init(struct AD5328_t* instance, float RefVoltageA, float RefVoltageB, uint8_t RefConfig);
int AD5328_SetVoltage(struct AD5328_t* instance, uint16_t channel, float voltage);
