#include <stdbool.h>
#include <stdlib.h>
#include <math.h>
#include "LL_spi.h"
#include "ad5328.h"

#define AD5328_SPI_MODE spmMode0

int AD5328_Init(struct AD5328_t* instance, float RefVoltageA, float RefVoltageB, uint8_t RefConfig)
{
	// Check if instance ref is assigned
	if (instance == NULL) return -1;
  	instance->VoltageADivider = (RefVoltageA / 4095.0);
  	instance->VoltageBDivider = (RefVoltageB / 4095.0);
	uint8_t confH = 0b10000000; //select Gain and reference config register
	uint8_t bytes[] = {confH, RefConfig};
	instance->SPIFunction(&(bytes[0]), 2, instance->ChipID, AD5328_SPI_MODE); //first send high byte
}

int AD5328_SetVoltage(struct AD5328_t* instance, uint16_t channel, float voltage)
{
    // Check if instance ref is assigned
    if (instance == NULL) return -1;
    // Check what reference voltage is used: Channel 0..3 = refA, Channel 4..7 = refB
    float refD = (channel > 3) ? instance->VoltageBDivider : instance->VoltageADivider;
    // Check if requested voltage is not lower than zero voltage
    if (voltage < 0) return -3;
    // Calculate the DAC value of the voltage
    uint16_t dacvalue = (uint16_t)round(voltage / refD);
	// Check if requested voltage is not higher than reference voltage
	if (dacvalue > 4095) return -2;
    // The 16-bit word consists of 1 control bit and 3 address bits followed by 8, 10, or 12 bits of DAC data, depending on the device
    // type.In the case of a DAC write, the MSB is a 0.The next 3
    // address bits determine whether the data is for DAC A, DAC B,
    // DAC C, DAC D, DAC E, DAC F, DAC G, or DAC H.
    uint16_t temp = (uint16_t)((channel << 12) | dacvalue);
    // Check if function pointer is assigned
    if (instance->SPIFunction == NULL) return -4;
    // Swap bytes and send
    uint8_t bytes[] = {(temp >> 8) & 0xff, temp & 0xff};
    instance->SPIFunction(&(bytes[0]), 2, instance->ChipID, AD5328_SPI_MODE); //first send high byte
   return 0;
}
